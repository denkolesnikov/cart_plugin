<?php

namespace Cart;


class Cart
{
    public $products;

    public function __construct()
    {
        if (isset($_COOKIE['cart'])) {
           $this->products = json_decode($_COOKIE['cart'], true);
        }
    }

    public function getProducts(){
        foreach ($this->products as $product){
            echo '<p>'.$product['title'].' - <b>'.$product['price'].'</b> '.$product['currency'].'</p>';
        }
    }

    public function addProduct($item){
        $this->products [] = $item;
    }

    public function saveCart(){
        setcookie('cart', json_encode($this->products), time()+3600);
    }


}