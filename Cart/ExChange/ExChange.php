<?php

namespace Cart\ExChange;

class ExChange
{
    static $currency = [

        'uah' => 1,

        'usd' => 28.06,

        'eur' => 33.49

    ];

    static function convert($value, $currIn , $currOut){

        $cur = self::$currency;
        $result = '';

       if(isset($cur[$currIn], $cur[$currOut])){

           if ($cur[$currIn] == 1){

               $result = $value / $cur[$currOut];

           }else if($cur[$currOut] == 1){

               $result = $value * $cur[$currIn];

           }else{
               $result = ($value * $cur[$currIn]) / $cur[$currOut];
           }

           $result = round($result , 2);

       }else{
           $result = 'unsupported currency';
       }
        return $result;
    }

}