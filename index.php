<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

use Cart\Cart;
use Cart\ExChange\ExChange;

function __autoload($class){
    $path = str_replace('\\', '/', $class).'.php';
    if(file_exists($path)){
        require $path;
    }
}


$products = [
    [
        'title' => 'Macbook',
        'price' =>40000,
        'currency' =>'uah'
    ],

    [
        'title' => 'iPhone',
        'price' =>600,
        'currency' =>'usd'
    ],

    [
        'title' => 'Asus',
        'price' =>800,
        'currency' =>'eur'
    ]
];


$myCart = new Cart();

$toCurrency = 'uah';

foreach ($products as $item){
    $item['price'] = ExChange::convert($item['price'],$item['currency'], $toCurrency);
    $item['currency'] = $toCurrency ;
    $myCart->addProduct($item);
}

$myCart->saveCart();

$myCart->getProducts();
